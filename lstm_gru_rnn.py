import keras
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix, recall_score, precision_score
from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM, Activation, GRU
from keras.layers import SimpleRNN as RNN

# Установка затравки для воспроизводимости
np.random.seed(1234)
PYTHONHASHSEED = 0

# Функция для преобразования объектов в выборки, временные шаги, особенности
def gen_sequence(id_df, seq_length, seq_cols):
    data_array = id_df[seq_cols].values
    num_elements = data_array.shape[0]
    for start, stop in zip(range(0, num_elements-seq_length), range(seq_length, num_elements)):
        yield data_array[start:stop, :]

# Функция для генерации меток (0 или 1)
def gen_labels(id_df, seq_length, label):
    data_array = id_df[label].values
    num_elements = data_array.shape[0]

    return data_array[seq_length:num_elements, :]

def LSTM_func (label_array, seq_array):
    nb_features = seq_array.shape[2]
    nb_out = label_array.shape[1]

    model = Sequential()

    # Первый слой
    model.add(LSTM(
            input_shape=(sequence_length, nb_features),
            units=100,
            return_sequences=True))
    # Второй слой (Решает проблему переобучения)
    model.add(Dropout(0.2))

    # Третий слой 
    model.add(LSTM(
            units=50,
            return_sequences=False))
    # Четвертый слой
    model.add(Dropout(0.2))
    # Пятый слой
    # Используем сигмовидную форму, потому что мы выполняем бинарную классификацию, поэтому использование "relu" будет некорректным
    model.add(Dense(units=nb_out, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())

    history = model.fit(seq_array, label_array, epochs=10, batch_size=200, validation_split=0.05, verbose=1)
    loss_values = history.history['loss']
    val_loss_values = history.history['val_loss']
    accuracy_values = history.history['accuracy']
    val_accuracy_values = history.history['val_accuracy']

    # Зависимость потерь от обучающих и валидационных выборок
    plt.figure(figsize=(10, 7))

    plt.subplot(1, 2, 1)

    plt.plot(range(1, len(loss_values) + 1), loss_values, label='Training sample loss', color="#00FFFF")
    plt.plot(range(1, len(val_loss_values) + 1), val_loss_values, label='Validation sample loss', color="#0000FF")

    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.grid()
    plt.title('Training and Validation samples loss')

    # Зависимость точности от обучающих и валидационных выборок
    plt.subplot(1, 2, 2)

    plt.plot(range(1, len(accuracy_values) + 1), accuracy_values, label='Training Accuracy', color="#00FFFF")
    plt.plot(range(1, len(val_accuracy_values) + 1), val_accuracy_values, label='Validation Accuracy', color="#0000FF")

    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.grid()
    plt.title('Training and Validation Accuracy')

    # plt.tight_layout()
    plt.show()

    scores = model.evaluate(seq_array, label_array, verbose=1, batch_size=200)
    print('Accurracy: {}'.format(scores[1]))

    # Прогнозирование
    y_pred = model.predict(seq_array, verbose=1, batch_size=200)
    y_pred = (y_pred > 0.5).astype("int32")
    y_true = label_array

    # Compute precision and recall
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    print( 'Precision:', precision, '\nRecall: ', recall)

    # Compute confusion matrix and show it
    print('\nConfusion matrix\n- x-axis is true labels.\n- y-axis is predicted labels')
    cm = confusion_matrix(y_true, y_pred)
    print(cm)

    # Для тестирования мы выбираем двигатель с id="9" и преобразуем из фрейма данных в массив numpy
    id = "9"
    seq_array_test_last = [test_df[test_df['id']==id][sequence_cols].values[-sequence_length:]
                        for id in test_df['id'].unique() if len(test_df[test_df['id']==id]) >= sequence_length]

    seq_array_test_last = np.asarray(seq_array_test_last).astype(np.float32)
    print(seq_array_test_last.shape)

    # Создание меток - установка 0 или 1
    y_mask = [len(test_df[test_df['id']==id]) >= sequence_length for id in test_df['id'].unique()]

    label_array_test_last = test_df.groupby('id')['label1'].nth(-1)[y_mask].values
    label_array_test_last = label_array_test_last.reshape(label_array_test_last.shape[0],1).astype(np.float32)

    print(label_array_test_last.shape)
    print(seq_array_test_last.shape)
    print(label_array_test_last.shape)

    # Test metrics
    scores_test = model.evaluate(seq_array_test_last, label_array_test_last, verbose=2)
    print(f"Accurracy: {scores_test[1]}")

    # Make predictions
    y_pred_test = model.predict(seq_array_test_last)
    y_pred_test = (y_pred_test > 0.5).astype("int32")
    y_true_test = label_array_test_last

    # Compute precision, recall and f1-score
    precision_test = precision_score(y_true_test, y_pred_test)
    recall_test = recall_score(y_true_test, y_pred_test)
    f1_test = 2 * (precision_test * recall_test) / (precision_test + recall_test)
    print( 'Precision: ', precision_test, '\nRecall: ', recall_test, '\nF1-score:', f1_test )

    # Compute confusion matrix
    print('\nConfusion matrix\n- x-axis is true labels\n- y-axis is predicted labels')
    cm = confusion_matrix(y_true_test, y_pred_test)
    print(cm)

def GRU_func (label_array, seq_array):
    nb_features = seq_array.shape[2]
    nb_out = label_array.shape[1]

    model = Sequential()

    # Первый слой
    model.add(GRU(
            input_shape=(sequence_length, nb_features),
            units=100,
            return_sequences=True))
    # Второй слой
    model.add(Dropout(0.2))

    # Третий слой
    model.add(GRU(
            units=50,
            return_sequences=False))
    # Четвертый слой
    model.add(Dropout(0.2))
    # Пятый слой
    model.add(Dense(units=nb_out, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    print(model.summary())

    history = model.fit(seq_array, label_array, epochs=10, batch_size=200, validation_split=0.05, verbose=1)

    loss_values = history.history['loss']
    val_loss_values = history.history['val_loss']
    accuracy_values = history.history['accuracy']
    val_accuracy_values = history.history['val_accuracy']

    # Зависимость потерь от обучающих и валидационных выборок
    plt.figure(figsize=(10, 7))

    plt.subplot(1, 2, 1)

    plt.plot(range(1, len(loss_values) + 1), loss_values, label='Training sample loss', color="#00FFFF")
    plt.plot(range(1, len(val_loss_values) + 1), val_loss_values, label='Validation sample loss', color="#0000FF")

    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.grid()
    plt.title('Training and Validation samples loss')

    # Зависимость точности от обучающих и валидационных выборок
    plt.subplot(1, 2, 2)

    plt.plot(range(1, len(accuracy_values) + 1), accuracy_values, label='Training Accuracy', color="#00FFFF")
    plt.plot(range(1, len(val_accuracy_values) + 1), val_accuracy_values, label='Validation Accuracy', color="#0000FF")

    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.grid()
    plt.title('Training and Validation Accuracy')

    # plt.tight_layout()
    plt.show()

    # Показатели обучения в поезде
    # Точность вычислений
    scores = model.evaluate(seq_array, label_array, verbose=1, batch_size=200)
    print('Accurracy: {}'.format(scores[1]))

    # Прогнозирование
    y_pred = model.predict(seq_array, verbose=1, batch_size=200)
    y_pred = (y_pred > 0.5).astype("int32")
    y_true = label_array

    # Compute precision and recall
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    print( 'Precision:', precision, '\nRecall: ', recall)

    # Compute confusion matrix and show it
    print('\nConfusion matrix\n- x-axis is true labels.\n- y-axis is predicted labels')
    cm = confusion_matrix(y_true, y_pred)
    print(cm)

    # Для тестирования мы выбираем двигатель с ID="9" и преобразуем из фрейма данных в массив numpy
    id = "9"
    seq_array_test_last = [test_df[test_df['id']==id][sequence_cols].values[-sequence_length:]
                        for id in test_df['id'].unique() if len(test_df[test_df['id']==id]) >= sequence_length]

    seq_array_test_last = np.asarray(seq_array_test_last).astype(np.float32)
    print(seq_array_test_last.shape)

    # Создание меток - установка 0 или 1
    y_mask = [len(test_df[test_df['id']==id]) >= sequence_length for id in test_df['id'].unique()]

    label_array_test_last = test_df.groupby('id')['label1'].nth(-1)[y_mask].values
    label_array_test_last = label_array_test_last.reshape(label_array_test_last.shape[0],1).astype(np.float32)

    print(label_array_test_last.shape)
    print(seq_array_test_last.shape)
    print(label_array_test_last.shape)

    # Test metrics
    scores_test = model.evaluate(seq_array_test_last, label_array_test_last, verbose=2)
    print(f"Accurracy: {scores_test[1]}")

    # Make predictions
    y_pred_test = model.predict(seq_array_test_last)
    y_pred_test = (y_pred_test > 0.5).astype("int32")
    y_true_test = label_array_test_last

    # Compute precision, recall and f1-score
    precision_test = precision_score(y_true_test, y_pred_test)
    recall_test = recall_score(y_true_test, y_pred_test)
    f1_test = 2 * (precision_test * recall_test) / (precision_test + recall_test)
    print( 'Precision: ', precision_test, '\nRecall: ', recall_test, '\nF1-score:', f1_test )

    # Compute confusion matrix
    print('\nConfusion matrix\n- x-axis is true labels.\n- y-axis is predicted labels')
    cm = confusion_matrix(y_true_test, y_pred_test)
    print(cm)

def RNN_func (label_array, seq_array):
    nb_features = seq_array.shape[2]
    nb_out = label_array.shape[1]

    model = Sequential()

    # First layer
    model.add(RNN(
            input_shape=(sequence_length, nb_features),
            units=100,
            return_sequences=True))
    # Second layer
    model.add(Dropout(0.2))

    # Third layer
    model.add(RNN(
            units=50,
            return_sequences=False))
    # Fourth layer
    model.add(Dropout(0.2))
    # Fifth layer
    model.add(Dense(units=nb_out, activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    print(model.summary())

    history = model.fit(seq_array, label_array, epochs=10, batch_size=200, validation_split=0.05, verbose=1)

    loss_values = history.history['loss']
    val_loss_values = history.history['val_loss']
    accuracy_values = history.history['accuracy']
    val_accuracy_values = history.history['val_accuracy']

    # Зависимость потерь от обучающих и валидационных выборок
    plt.figure(figsize=(10, 7))

    plt.subplot(1, 2, 1)

    plt.plot(range(1, len(loss_values) + 1), loss_values, label='Training sample loss', color="#00FFFF")
    plt.plot(range(1, len(val_loss_values) + 1), val_loss_values, label='Validation sample loss', color="#0000FF")

    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.grid()
    plt.title('Training and Validation samples loss')

    # Зависимость точности от обучающих и валидационных выборок
    plt.subplot(1, 2, 2)

    plt.plot(range(1, len(accuracy_values) + 1), accuracy_values, label='Training Accuracy', color="#00FFFF")
    plt.plot(range(1, len(val_accuracy_values) + 1), val_accuracy_values, label='Validation Accuracy', color="#0000FF")

    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.grid()
    plt.title('Training and Validation Accuracy')

    # plt.tight_layout()
    plt.show()

    # Показатели обучения в поезде
    # Точность вычислений
    scores = model.evaluate(seq_array, label_array, verbose=1, batch_size=200)
    print('Accurracy: {}'.format(scores[1]))

    # Making predicti ons
    y_pred = model.predict(seq_array, verbose=1, batch_size=200)
    y_pred = (y_pred > 0.5).astype("int32")
    y_true = label_array

    # Compute precision and recall
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    print( 'Precision:', precision, '\nRecall: ', recall)

    # Compute confusion matrix and show it
    print('\nConfusion matrix\n- x-axis is true labels.\n- y-axis is predicted labels')
    cm = confusion_matrix(y_true, y_pred)
    print(cm)

    # Для теста мы выбираем движок с ID="9" и преобразуем из фрейма данных в массив numpy
    id = "9"
    seq_array_test_last = [test_df[test_df['id']==id][sequence_cols].values[-sequence_length:]
                        for id in test_df['id'].unique() if len(test_df[test_df['id']==id]) >= sequence_length]

    seq_array_test_last = np.asarray(seq_array_test_last).astype(np.float32)
    seq_array_test_last.shape

    # Создание меток - установка 0 или 1
    y_mask = [len(test_df[test_df['id']==id]) >= sequence_length for id in test_df['id'].unique()]

    label_array_test_last = test_df.groupby('id')['label1'].nth(-1)[y_mask].values
    label_array_test_last = label_array_test_last.reshape(label_array_test_last.shape[0],1).astype(np.float32)

    print(label_array_test_last.shape)
    print(seq_array_test_last.shape)
    print(label_array_test_last.shape)

    # Test metrics
    scores_test = model.evaluate(seq_array_test_last, label_array_test_last, verbose=2)
    print('Accurracy: {}'.format(scores_test[1]))

    # Make predictions
    y_pred_test = model.predict(seq_array_test_last)
    y_pred_test = (y_pred_test > 0.5).astype("int32")
    y_true_test = label_array_test_last

    # Compute precision, recall and f1-score
    precision_test = precision_score(y_true_test, y_pred_test)
    recall_test = recall_score(y_true_test, y_pred_test)
    f1_test = 2 * (precision_test * recall_test) / (precision_test + recall_test)
    print( 'Precision: ', precision_test, '\nRecall: ', recall_test, '\nF1-score:', f1_test )

    # Compute confusion matrix
    print('\nConfusion matrix\n- x-axis is true labels.\n- y-axis is predicted labels')
    cm = confusion_matrix(y_true_test, y_pred_test)
    print(cm)

# --------------------------------------------------------------------


# Чтение training данных
train_df = pd.read_csv('./data/PM_train.txt', sep=" ", header=None)
train_df.drop(train_df.columns[[26, 27]], axis=1, inplace=True)
train_df.columns = ['id', 'cycle', 'setting1', 'setting2', 'setting3', 's1', 's2', 's3',
                     's4', 's5', 's6', 's7', 's8', 's9', 's10', 's11', 's12', 's13', 's14',
                     's15', 's16', 's17', 's18', 's19', 's20', 's21']

# Чтение test данных
test_df = pd.read_csv('./data/PM_test.txt', sep=" ", header=None)
test_df.drop(test_df.columns[[26, 27]], axis=1, inplace=True)
test_df.columns = ['id', 'cycle', 'setting1', 'setting2', 'setting3', 's1', 's2', 's3',
                     's4', 's5', 's6', 's7', 's8', 's9', 's10', 's11', 's12', 's13', 's14',
                     's15', 's16', 's17', 's18', 's19', 's20', 's21']

# Чтение GTD
truth_df = pd.read_csv('./data/PM_truth.txt', sep=" ", header=None)
truth_df.drop(truth_df.columns[[1]], axis=1, inplace=True)

# Сортировка датасета по id и cycle
train_df = train_df.sort_values(['id','cycle'])
#print(train_df)

# Генерация значений столбца RUL (оставшийся срок полезного использования)
rul = pd.DataFrame(train_df.groupby('id')['cycle'].max()).reset_index()
rul.columns = ['id', 'max']
train_df = train_df.merge(rul, on=['id'], how='left')
train_df['RUL'] = train_df['max'] - train_df['cycle']
train_df.drop('max', axis=1, inplace=True)
#print(train_df)

# Генерация столбцы меток для обучающих данных 
w1 = 30
train_df['label1'] = np.where(train_df['RUL'] <= w1, 1, 0 )
#print(train_df)

# MinMax нормализация
train_df['cycle_norm'] = train_df['cycle']
cols_normalize = train_df.columns.difference(['id','cycle','RUL','label1','label2'])

min_max_scaler = preprocessing.MinMaxScaler()

norm_train_df = pd.DataFrame(min_max_scaler.fit_transform(train_df[cols_normalize]),
                             columns=cols_normalize,
                             index=train_df.index)

join_df = train_df[train_df.columns.difference(cols_normalize)].join(norm_train_df)
train_df = join_df.reindex(columns = train_df.columns)

#print(train_df)

# Нормализация тестовых данных
test_df['cycle_norm'] = test_df['cycle']

norm_test_df = pd.DataFrame(min_max_scaler.transform(test_df[cols_normalize]),
                            columns=cols_normalize,
                            index=test_df.index)

test_join_df = test_df[test_df.columns.difference(cols_normalize)].join(norm_test_df)
test_df = test_join_df.reindex(columns = test_df.columns)
test_df = test_df.reset_index(drop=True)

#print(test_df)

# Сгенерировать столбец max для тестовых данных
# max - максимально возможное количество циклов до выхода двигателя из строя
rul = pd.DataFrame(test_df.groupby('id')['cycle'].max()).reset_index()
rul.columns = ['id', 'max']

truth_df.columns = ['more']
truth_df['id'] = truth_df.index + 1
truth_df['max'] = rul['max'] + truth_df['more']
truth_df.drop('more', axis=1, inplace=True)

#print(truth_df)

# Генерация RUL для test данных
test_df = test_df.merge(truth_df, on=['id'], how='left')
test_df['RUL'] = test_df['max'] - test_df['cycle']
test_df.drop('max', axis=1, inplace=True)

#print(test_df)

# Сгенерируйте столбец меток w1 для тестовых данных (0, если он не сломается, и 1, если он сломается)
test_df['label1'] = np.where(test_df['RUL'] <= w1, 1, 0 )

#print(test_df)

# Выбор размера окна в 50 циклов
sequence_length = 50

# Подготовка данных для визуализации
# Интервал в 50 циклов до точки отказа для идентификатора двигателя 9 - по номеру варианта задания
engine_id3 = test_df[test_df['id'] == 9]
engine_id3_50cycleWindow = engine_id3[engine_id3['RUL'] <= engine_id3['RUL'].min() + sequence_length]
cols1 = ['s1', 's2', 's3', 's4', 's5', 's6', 's7', 's8', 's9', 's10']
engine_id3_50cycleWindow1 = engine_id3_50cycleWindow[cols1]
cols2 = ['s11', 's12', 's13', 's14', 's15', 's16', 's17', 's18', 's19', 's20', 's21']
engine_id3_50cycleWindow2 = engine_id3_50cycleWindow[cols2]

# Отображение данных датчиков для идентификатора двигателя 9 до точки отказа - датчики 1-10
ax1 = engine_id3_50cycleWindow2.plot(subplots=True, sharex=True, figsize=(10, 7))
plt.show()
# Отображение данных датчиков для идентификатора двигателя 9 до точки отказа - датчики 11-21
ax2 = engine_id3_50cycleWindow2.plot(subplots=True, sharex=True, figsize=(10, 7))
plt.show()

# Выбор столбцов объектов
sensor_cols = ['s' + str(i) for i in range(1,22)]
sequence_cols = ['setting1', 'setting2', 'setting3', 'cycle_norm']
sequence_cols.extend(sensor_cols)

# Генератор для последовательностей
seq_gen = (list(gen_sequence(train_df[train_df['id']==id], sequence_length, sequence_cols))
           for id in train_df['id'].unique())

# Генерация последовательности и преобразование в массив numpy
seq_array = np.concatenate(list(seq_gen)).astype(np.float32)

# Генерация маркеров
label_gen = [gen_labels(train_df[train_df['id']==id], sequence_length, ['label1'])
             for id in train_df['id'].unique()]
label_array = np.concatenate(label_gen).astype(np.float32)
label_array.shape

#LSTM_func(label_array, seq_array)
#GRU_func(label_array, seq_array)
#RNN_func(label_array, seq_array)